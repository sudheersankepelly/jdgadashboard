﻿using ClosedXML.Excel;
using JdJobsDashBoard.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.OracleClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using System.Net.Mail;
using System.Globalization;

namespace JdJobsDashBoard.Controllers
{
    public class TaskController : Controller
    {
        string status = string.Empty;
        //string ftpCsvCreatedTime = string.Empty;
        DataTable dt = new DataTable();

        DateTime lastruntimedt;
        string strtablename = string.Empty;
        string strcolumnname = string.Empty;
        string strq = string.Empty;
        // GET: Task
        public ActionResult Index()
        {
            dt = ReadCsvFTPFile();

            if (dt != null)
            {
                List<TaskInfo> TaskInfoList = new List<TaskInfo>();
                //string strqueryy= GetQuery(strtablename, strcolumnname);
                // List<TaskInfo> LstGaReport = GetMCRMData();

                //   dt.Columns.Add("TABLE_NAME", typeof(String));

                dt.Columns.Add("Total", typeof(String));

                foreach (DataRow dr in dt.Rows)
                {
                    TaskInfo tf = new TaskInfo();
                    tf.folder = dr["folder"].ToString();
                    tf.name = dr["name"].ToString();
                    tf.logFilePath = dr["logFilePath"].ToString();
                    tf.lastRunTime = dr["lastRunTime"].ToString();
                    tf.nextRunTime = dr["nextRunTime"].ToString();
                    tf.state = dr["state"].ToString();
                    tf.logInfo = dr["logInfo"].ToString();
                    tf.tablename = dr["TABLENAME"].ToString();
                    tf.schema = dr["schema"].ToString();
                    tf.total = GetTableName(tf.tablename, tf.schema) + "\n";
                    //lastruntimedt = Convert.ToDateTime(tf.lastRunTime);
                    CultureInfo us = new CultureInfo("en-US");
                    lastruntimedt = Convert.ToDateTime(tf.lastRunTime, us);
                    TaskInfoList.Add(tf);

                }
                //var list = (from x in TaskInfoList join y in LstGaReport on x.name equals y.tablename select new { x.folder, x.name, x.logFilePath, x.lastRunTime, x.nextRunTime, x.state, x.logInfo, y.tablename, y.total }).ToList();
                //foreach (var item in list)
                //{
                //    TaskInfo tTaskInfo = new TaskInfo();
                //    tTaskInfo.folder = item.folder;//dt.Rows[i]["folder"].ToString();
                //    tTaskInfo.name = item.name;//dt.Rows[i]["name"].ToString();
                //    tTaskInfo.logFilePath = item.logFilePath;//dt.Rows[i]["logFilePath"].ToString();
                //    tTaskInfo.lastRunTime = item.lastRunTime;//dt.Rows[i]["lastRunTime"].ToString();
                //    tTaskInfo.nextRunTime = item.nextRunTime;//dt.Rows[i]["nextRunTime"].ToString();
                //    tTaskInfo.state = item.state;//dt.Rows[i]["state"].ToString();
                //    tTaskInfo.logInfo = item.logInfo;//dt.Rows[i]["logInfo"].ToString();
                //    tTaskInfo.tablename = item.tablename;//dt.Rows[i]["TABLE_NAME"].ToString();
                //    tTaskInfo.total = item.total;//dt.Rows[i]["Total"].ToString();
                //    //DataTable table = ConvertListToDataTable(LstGaReport);
                //    //tTaskInfo.tablename = table.Rows[i]["TABLE_NAME"].ToString();
                //    //tTaskInfo.total = table.Rows[i]["Total"].ToString();
                //    TaskInfoList.Add(tTaskInfo);
                //}
                //for (int i = 0; i < dt.Rows.Count; i++)
                //{
                //    TaskInfo tTaskInfo = new TaskInfo();
                //    tTaskInfo.folder = dt.Rows[i]["folder"].ToString();
                //    tTaskInfo.name = dt.Rows[i]["name"].ToString();
                //    tTaskInfo.logFilePath = dt.Rows[i]["logFilePath"].ToString();
                //    tTaskInfo.lastRunTime = dt.Rows[i]["lastRunTime"].ToString();
                //    tTaskInfo.nextRunTime = dt.Rows[i]["nextRunTime"].ToString();
                //    tTaskInfo.state = dt.Rows[i]["state"].ToString();
                //    tTaskInfo.logInfo = dt.Rows[i]["logInfo"].ToString();
                //    //tTaskInfo.tablename = dt.Rows[i]["TABLE_NAME"].ToString();
                //    //tTaskInfo.total = dt.Rows[i]["Total"].ToString();
                //    //DataTable table = ConvertListToDataTable(LstGaReport);
                //    //tTaskInfo.tablename = table.Rows[i]["TABLE_NAME"].ToString();
                //    //tTaskInfo.total = table.Rows[i]["Total"].ToString();
                //    TaskInfoList.Add(tTaskInfo);


                //}

                return View(TaskInfoList);
            }

            ViewBag.error = status;
            SendMailReport();

            return View();
        }
        public string GetTableName(string tablname, string strschema)
        {
            try
            {
                if (tablname != "")
                {
                    string strResult = string.Empty;
                    var strOtable = tablname.ToString().Split(';');
                    //for (int k = 0; k < strOtable.Length; k++)
                    foreach (string str in strOtable)
                    {
                        var y = str.Split(':');

                        strtablename = y[0];
                        if (y.Count() > 1)
                            strcolumnname = y[1];
                        strq = GetQuery(strtablename, strcolumnname, strschema);
                        if (strq != string.Empty)
                            strResult = strResult + strtablename + " : " + GetMCRMData(strq) + "\n";


                    }

                    return strResult;
                }
                else
                    return string.Empty;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }



        [NonAction]
        public DataTable ReadCsvFTPFile()
        {
            try
            {
                DataTable oDataTable = null;
                List<TaskInfo> lstTaskinfo = new List<TaskInfo>();
                string ftpserver = ConfigurationManager.AppSettings["FTPUrl"]; // "ftp://10.32.49.77/";

                FtpWebRequest ftprequestfile = (FtpWebRequest)FtpWebRequest.Create(new Uri(ftpserver));
                ftprequestfile.Credentials = new NetworkCredential(ConfigurationManager.AppSettings["FTPUserName"], ConfigurationManager.AppSettings["FTPPassword"]);
                ftprequestfile.Method = WebRequestMethods.Ftp.DownloadFile;
                using (var response = (FtpWebResponse)ftprequestfile.GetResponse())
                {
                    Stream responseStream = response.GetResponseStream();
                    StreamReader oStreamReader = new StreamReader(responseStream);
                    //ViewBag.ftpCsvCreatedTime = Convert.ToDateTime(response.LastModified).ToString();
                    int RowCount = 0;
                    string[] ColumnNames = null;
                    string[] oStreamDataValues = null;
                    //using while loop read the stream data till end
                    while (!oStreamReader.EndOfStream)
                    {
                        string oStreamRowData = oStreamReader.ReadLine().Trim();
                        if (oStreamRowData.Length > 0)
                        {
                            oStreamDataValues = oStreamRowData.Split(',');
                            //Bcoz the first row contains column names, we will poluate 
                            //the column name by
                            //reading the first row and RowCount-0 will be true only once
                            if (RowCount == 0)
                            {
                                RowCount = 1;
                                ColumnNames = oStreamRowData.Split(',');
                                oDataTable = new DataTable();

                                //using foreach looping through all the column names
                                foreach (string csvcolumn in ColumnNames)
                                {
                                    DataColumn oDataColumn = new DataColumn(csvcolumn.ToUpper(), typeof(string));

                                    //setting the default value of empty.string to newly created column
                                    oDataColumn.DefaultValue = string.Empty;

                                    //adding the newly created column to the table
                                    oDataTable.Columns.Add(oDataColumn);
                                }
                            }
                            else
                            {
                                //creates a new DataRow with the same schema as of the oDataTable            
                                DataRow oDataRow = oDataTable.NewRow();

                                //using foreach looping through all the column names
                                for (int i = 0; i < ColumnNames.Length; i++)
                                {
                                    oDataRow[i] = oStreamDataValues[i] == null ? string.Empty : oStreamDataValues[i].ToString();
                                }

                                //adding the newly created row with data to the oDataTable       
                                oDataTable.Rows.Add(oDataRow);
                            }
                        }
                    }
                    return oDataTable;
                }
            }
            catch (WebException ex)
            {
                FtpWebResponse response = (FtpWebResponse)ex.Response;
                if (response.StatusCode == FtpStatusCode.ActionNotTakenFileUnavailable)
                {
                    response.Close();
                }
                else
                {
                    response.Close();
                }
                status = ((FtpWebResponse)ex.Response).StatusDescription;
                return null;
            }
        }

        [HttpGet]
        [ActionName("ExportToExcel")]
        public FileContentResult ExportToExcel()
        {
            //Codes for the Closed XML
            using (XLWorkbook wb = new XLWorkbook())
            {
                wb.Worksheets.Add(dt);
                using (MemoryStream stream = new MemoryStream())
                {
                    wb.SaveAs(stream);
                    return File(stream.ToArray(), "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "Jobslist.xlsx");
                }
            }
        }

        [NonAction]
        public MemoryStream GetStream(XLWorkbook excelWorkbook)
        {
            MemoryStream fs = new MemoryStream();
            excelWorkbook.SaveAs(fs);
            fs.Position = 0;
            return fs;
        }


        static private string GetConnectionString()
        {
            // To avoid storing the connection string in your code, 
            // you can retrieve it from a configuration file. 
            return ConfigurationManager.AppSettings["MCRMSTGConnection"];
        }
        static private string GetGAConnectionString()
        {
            // To avoid storing the connection string in your code, 
            // you can retrieve it from a configuration file. 
            return ConfigurationManager.AppSettings["GABIPRDConnection"];
        }
        public string tableName = "";
        public string colName = "";
        public string GetQuery(string tableName, string colName, string strrschema)
        {
            //string strDate = DateTime.Now.ToString("dd-MMM-yyyy");
            //string strQuery = @"SELECT CallMediaImport' AS TABLE_NAME, 
            //count(ID) AS Total from DBO_WEBCOMMERCEINTERNAL.AMPLIANCE_ASSET_DETAILS
            //where   to_char(LASTUPDATEDON) = 'DD-MON-YYYY'";

            //string strQry = Regex.Replace(strQuery, "DD-MON-YYYY", strDate, RegexOptions.IgnoreCase);
            //return strQry;

            //string strDate = DateTime.Now.ToString("dd-MMM-yy").ToUpper();

            string strDate = lastruntimedt.ToString("dd-MMM-yy").ToUpper();
            string strQuery = @"SELECT '" + tableName.ToString() + "' AS TABLE_NAME" + "," +
            "count(*) AS Total from " + strrschema + ".";
            string strq = strQuery + tableName + " where" + " " + "to_char(" + colName.ToString() + ")" + "='DD-MON-YYYY'";
            string strQry = Regex.Replace(strq, "DD-MON-YYYY", strDate, RegexOptions.IgnoreCase);
            return strQry;

        }

        TaskInfo taskinf = new TaskInfo();
        private string GetMCRMData(string strSql)
        {
            string connectionString = "";
            string strResult = string.Empty;
            if (strSql.Contains("GABIPRD_GENERAL")) { connectionString = GetGAConnectionString(); } else { connectionString = GetConnectionString(); }

            using (OracleConnection connection = new OracleConnection())
            {
                connection.ConnectionString = connectionString;
                connection.Open();
                Console.WriteLine("State: {0}", connection.State);
                Console.WriteLine("ConnectionString: {0}",
                                  connection.ConnectionString);

                OracleCommand command = connection.CreateCommand();

                command.CommandText = strSql;

                OracleDataReader dr = command.ExecuteReader();
                if (dr != null && dr.HasRows)
                {
                    while (dr.Read())
                    {
                        //LstGaReport.Add(
                        //    new TaskInfo { tablename = dr["TABLE_NAME"].ToString(), total = dr["Total"].ToString() });
                        strResult = dr["Total"].ToString();

                    }
                }
                //if (LstGaReport.Count > 0)
                //{
                //    return LstGaReport;
                //}
                //else
                //{
                //    return null;
                //}
                return strResult;
            }
        }

        private void SendMailReport()
        {
            try
            {
                MailMessage message = new MailMessage
                {
                    Subject = ConfigurationManager.AppSettings["subject"],
                    From = new MailAddress(ConfigurationManager.AppSettings["senderMailAddress"], ConfigurationManager.AppSettings["senderName"])
                };

                message.Priority = MailPriority.High;
                message.Body = ConfigurationManager.AppSettings["mailContent"];
                //message.Body += ex.Message;


                SmtpClient smptClient = new SmtpClient(ConfigurationManager.AppSettings["mailServer"], Convert.ToInt16(ConfigurationManager.AppSettings["mailServerPort"]));
                System.Net.NetworkCredential credentials = new System.Net.NetworkCredential();
                credentials.UserName = ConfigurationManager.AppSettings["userName"];
                credentials.Password = ConfigurationManager.AppSettings["password"];
                //smtp.UseDefaultCredentials = true;
                smptClient.Credentials = credentials;
                smptClient.EnableSsl = true;
                smptClient.Send(message);

            }
            catch (Exception)
            {

                throw;
            }
        }


    }
}