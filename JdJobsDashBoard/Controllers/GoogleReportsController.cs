﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data;
using System.Data.OleDb;
using System.Data.OracleClient;
using System.Configuration;
using System.Text.RegularExpressions;
using JdJobsDashBoard.Models;
using System.Security.Principal;
using System.Runtime.InteropServices;
using DocumentFormat.OpenXml.Drawing;
using System.IO;
using System.Net;
using System.Text;
using System.Collections.ObjectModel;
using System.Management.Automation;
using System.Security;
using System.Diagnostics;

namespace JdJobsDashBoard.Controllers
{
    public class GoogleReportsController : Controller
    {
        LibraryContext objContext = new LibraryContext();
        // GET: GoogleReports
        public ActionResult Index()

        {

            List<Sites> publishers = objContext.getSites.ToList();
            ViewBag.Sites = publishers;

            //string process = "";
            bool Isprocess = false;
            var proc = System.Diagnostics.Process.GetProcesses().ToArray();

            foreach (var item in proc)
            {
                if (item.ProcessName == "JD.FeedManager.GoogleAnalytics.SqlLoaderRunner" || item.ProcessName == "JD.FeedManager.GoogleAnalytics")
                    //process += "'" + item.ProcessName + "'" + " , ";
                    Isprocess = true;
            }
            ViewBag.process = Isprocess;
            var items = GetGaData();//GetGaData();
            if (items != null && items.Count > 0)
            {
                ViewBag.GaItems = items;
                return View(ViewBag);
            }
            else
                return View();

        }

        private string IsProcessRunning(string sProcessName)
        {
            System.Diagnostics.Process[] proc = System.Diagnostics.Process.GetProcessesByName(sProcessName);
            if (proc.Length > 0)
            {
                return String.Format("{0}is  running!", sProcessName);
            }
            else
            {
                return String.Format("{0}is not running!", sProcessName);
                // start your process
            }
        }

        static private string GetConnectionString()
        {
            // To avoid storing the connection string in your code, 
            // you can retrieve it from a configuration file. 
            return ConfigurationManager.AppSettings["GABIPRDConnection"];
        }

        static private string GetQuery()
        {
            string strDate = DateTime.Now.AddDays(-1).ToString("dd-MMM-yyyy");

            string strQuery = @"SELECT
                                'Topline' AS TABLE_NAME
                                ,SUM(T.TRANSACTIONREVENUE) AS TRANS_REVENUE,
                                count (t.profileid) as rowcount
                                from
                                GOOGAN.TOPLINE T
                                INNER JOIN GOOGAN.PROFILEID_LOOKUP P ON T.PROFILEID = P.PROFILEID
                                where
                                APPLICABLEDATE = trunc(sysdate)-1
                                AND P.SITE = 'JD'

                                UNION ALL

                                SELECT
                                'PrimaryDetails' AS TABLE_NAME
                                ,SUM(T.TRANSACTIONREVENUE) AS TRANS_REVENUE
                                ,count (t.profileid) as rowcount
                                FROM
                                GOOGAN.PRIMARYDETAILS T
                                INNER JOIN GOOGAN.PROFILEID_LOOKUP P ON T.PROFILEID = P.PROFILEID
                                where
                                APPLICABLEDATE = trunc(sysdate)-1
                                AND P.SITE = 'JD'

                                UNION ALL

                                SELECT
                                'LCPrimaryDetails' AS TABLE_NAME
                                ,SUM(T.TRANSACTIONREVENUE) AS TRANS_REVENUE
                                ,count (t.profileid) as rowcount
                                FROM
                                GOOGAN.LOCALCURRENCY_PRIMARYDETAILS T
                                INNER JOIN GOOGAN.PROFILEID_LOOKUP P ON T.PROFILEID = P.PROFILEID
                                where
                                REPORTDAY = trunc(sysdate)-1
                                AND P.SITE = 'JD'

                                UNION ALL

                                SELECT
                                'Primaryextended' AS TABLE_NAME
                                ,SUM(T.TRANSACTIONSREVENUE) AS TRANS_REVENUE
                                ,count (t.profileid) as rowcount
                                FROM
                                GOOGAN.Primaryextended T
                                INNER JOIN GOOGAN.PROFILEID_LOOKUP P ON T.PROFILEID = P.PROFILEID
                                where
                                REPORTDAY = trunc(sysdate)-1
                                AND P.SITE = 'JD'

                                Union all

                                SELECT
                                'FULLFUNNEL' AS TABLE_NAME
                                ,0 AS TRANS_REVENUE
                                ,count (t.profileid) as rowcount
                                FROM
                                GOOGAN.FULLFUNNEL T
                                INNER JOIN GOOGAN.PROFILEID_LOOKUP P ON T.PROFILEID = P.PROFILEID
                                where
                                REPORTDAY = trunc(sysdate)-1
                                AND P.SITE = 'JD'

                                Union all

                                SELECT
                                'FUNNELBYHOUR' AS TABLE_NAME
                                ,0 AS TRANS_REVENUE
                                ,count (t.profileid) as rowcount
                                FROM
                                GOOGAN.FUNNELBYHOUR T
                                INNER JOIN GOOGAN.PROFILEID_LOOKUP P ON T.PROFILEID = P.PROFILEID
                                where
                                REPORTDAY = trunc(sysdate)-1
                                AND P.SITE = 'JD'


                                Union all

                                SELECT
                                'EXPERIMENTS' AS TABLE_NAME
                                ,0 AS TRANS_REVENUE
                                ,count (t.profileid) as rowcount
                                FROM
                                GOOGAN.EXPERIMENTS T
                                INNER JOIN GOOGAN.PROFILEID_LOOKUP P ON T.PROFILEID = P.PROFILEID
                                where
                                REPORTDAY = trunc(sysdate)-1
                                AND P.SITE = 'JD'

                                Union all

                                SELECT
                                'EVENTERRORS' AS TABLE_NAME
                                ,0 AS TRANS_REVENUE
                                ,count (t.profileid) as rowcount
                                FROM
                                GOOGAN.EVENTERRORS T
                                INNER JOIN GOOGAN.PROFILEID_LOOKUP P ON T.PROFILEID = P.PROFILEID
                                where
                                REPORTDAY = trunc(sysdate)-1
                                AND P.SITE = 'JD'

                                UNION ALL

                                SELECT
                                'ToplineByDevice' AS TABLE_NAME
                                ,SUM(T.TRANSACTIONREVENUE) AS TRANS_REVENUE
                                ,count (t.profileid) as rowcount
                                FROM
                                GOOGAN.TOPLINEBYDEVICE T
                                INNER JOIN GOOGAN.PROFILEID_LOOKUP P ON T.PROFILEID = P.PROFILEID
                                where
                                APPLICABLEDATE = trunc(sysdate)-1
                                AND P.SITE = 'JD'

                                UNION ALL

                                SELECT
                                'OrderID_LookUp' AS TABLE_NAME
                                ,SUM(T.TRANSACTIONREVENUE) AS TRANS_REVENUE
                                ,count (t.profileid) as rowcount
                                FROM
                                GOOGAN.OrderID_LookUp T
                                INNER JOIN GOOGAN.PROFILEID_LOOKUP P ON T.PROFILEID = P.PROFILEID
                                where
                                APPLICABLEDATE = trunc(sysdate)-1
                                AND P.SITE = 'JD'

                                UNION ALL

                                SELECT
                                'TechnicalDetails' AS TABLE_NAME
                                ,SUM(T.TRANSACTIONREVENUE) AS TRANS_REVENUE
                                ,count (t.profileid) as rowcount
                                FROM
                                GOOGAN.TechnicalDetails T
                                INNER JOIN GOOGAN.PROFILEID_LOOKUP P ON T.PROFILEID = P.PROFILEID
                                where
                                APPLICABLEDATE = trunc(sysdate)-1
                                AND P.SITE = 'JD'

                                UNION ALL

                                SELECT
                                'LandingPagePerformance' AS TABLE_NAME
                                ,SUM(T.TRANSACTIONREVENUE) AS TRANS_REVENUE
                                ,count (t.profileid) as rowcount
                                FROM
                                GOOGAN.LandingPagePerformance T
                                INNER JOIN GOOGAN.PROFILEID_LOOKUP P ON T.PROFILEID = P.PROFILEID
                                where
                                APPLICABLEDATE = trunc(sysdate)-1
                                AND P.SITE = 'JD'

                                UNION ALL

                                SELECT
                                'LocalCurrencyTopline' AS TABLE_NAME
                                ,SUM(T.TRANSACTIONREVENUE) AS TRANS_REVENUE
                                ,count (t.profileid) as rowcount
                                FROM
                                GOOGAN.LOCALCURRENCY_TOPLINE T
                                INNER JOIN GOOGAN.PROFILEID_LOOKUP P ON T.PROFILEID = P.PROFILEID
                                where
                                REPORTDAY = trunc(sysdate)-1
                                AND P.SITE = 'JD'

                                UNION ALL
                                
                                SELECT
                                'LocalCurrencyToplineByDevice' AS TABLE_NAME
                                ,SUM(T.TRANSACTIONREVENUE) AS TRANS_REVENUE
                                ,count (t.profileid) as rowcount
                                FROM
                                GOOGAN.LOCALCURRENCY_TOPLINEBYDEVICE T
                                INNER JOIN GOOGAN.PROFILEID_LOOKUP P ON T.PROFILEID = P.PROFILEID
                                where
                                REPORTDAY = trunc(sysdate)-1
                                AND P.SITE = 'JD'

                                UNION ALL

                                SELECT
                                'ToplineByMinute' AS TABLE_NAME
                                ,SUM(T.TRANSACTIONREVENUE) AS TRANS_REVENUE
                                ,count (t.profileid) as rowcount
                                FROM
                                GOOGAN.TOPLINEBYMINUTE T
                                INNER JOIN GOOGAN.PROFILEID_LOOKUP P ON T.PROFILEID = P.PROFILEID
                                where
                                APPLICABLEDATE = trunc(sysdate)-1
                                AND P.SITE = 'JD'
                                UNION ALL

                                SELECT
                                'GOALS' AS TABLE_NAME
                                ,0 AS TRANS_REVENUE
                                 ,count ( t.profileid) as rowcount
                                FROM
                                GOOGAN.Goals T
                                INNER JOIN GOOGAN.PROFILEID_LOOKUP P ON T.PROFILEID = P.PROFILEID
                                where
                                APPLICABLEDATE = trunc(sysdate)-1
                                AND P.SITE = 'JD'
                                
                                UNION ALL

                                SELECT
                                 'PRODUCTDETAILS' AS TABLE_NAME
                                ,0 AS TRANS_REVENUE
                                 ,count ( t.profileid) as rowcount
                                FROM
                                GOOGAN.PRODUCTDETAILS T
                                INNER JOIN GOOGAN.PROFILEID_LOOKUP P ON T.PROFILEID = P.PROFILEID
                                where
                                APPLICABLEDATE = trunc(sysdate)-1
                                AND P.SITE = 'JD'";

            string strQry = Regex.Replace(strQuery, "DD-MON-YYYY", strDate, RegexOptions.IgnoreCase);
            return strQry;

        }

        // This will open the connection and query the database
        private List<GAReport> GetGaData()
        {
            List<GAReport> LstGaReport = new List<GAReport>();
            string connectionString = GetConnectionString();
            string sql = GetQuery();
            using (OracleConnection connection = new OracleConnection())
            {
                connection.ConnectionString = connectionString;
                connection.Open();
                Console.WriteLine("State: {0}", connection.State);
                Console.WriteLine("ConnectionString: {0}",
                                  connection.ConnectionString);

                OracleCommand command = connection.CreateCommand();

                command.CommandText = sql;

                OracleDataReader dr = command.ExecuteReader();
                if (dr != null && dr.HasRows)
                {
                    while (dr.Read())
                    {
                        LstGaReport.Add(
                            new GAReport { tableName = dr["TABLE_NAME"].ToString(), transRevenue = dr["TRANS_REVENUE"].ToString(), RowCount = Convert.ToInt32(dr["RowCount"]) });
                    }
                }
                if (LstGaReport.Count > 0)
                {
                    return LstGaReport;
                }
                else
                {
                    return null;
                }
            }
        }

        //Re-Run NightlyReport 
        public JsonResult RunPS1File(string[] tableName)
        {
            string message = string.Empty;
            string text = string.Empty;
            string sourcePath = ConfigurationManager.AppSettings["sourcePath"].ToString();
            string targetPath = ConfigurationManager.AppSettings["targetPath"].ToString();
            string fileName = string.Empty;
            string destFile = string.Empty;
            try
            {
                // To copy all the files in one directory to another directory.             
                if (System.IO.Directory.Exists(sourcePath))
                {
                    string[] files = System.IO.Directory.GetFiles(sourcePath);
                    // Copy the files and overwrite destination files if they already exist. 
                    foreach (string s in files)
                    {
                        // Use static Path methods to extract only the file name from the path.
                        fileName = System.IO.Path.GetFileName(s);
                        destFile = System.IO.Path.Combine(targetPath, fileName);
                        //  System.IO.File.Delete(destFile);
                        System.IO.File.Copy(s, destFile, true);
                    }
                }
                else
                {
                    message = "Source path does not exist!";
                }

                //Add checkbox checked text to PS1 file 
                string writePath = ConfigurationManager.AppSettings["WritePS1"].ToString();
                string path = Server.MapPath(writePath);
                var fileStream = new FileStream(path, FileMode.Open, FileAccess.Read);

                using (var streamReader = new StreamReader(fileStream, Encoding.UTF8))
                {
                    var lines = streamReader.ReadToEnd().Split("\r\n".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
                    foreach (var line in lines)
                    {

                        if (line.Trim().Contains("###WriteLineHere"))
                        {
                            foreach (var item in tableName)
                            {
                                if (item.ToLower() == "topline")
                                {
                                    text += "       & $GoogleAnalyticsAppPath DailyToplineCsvReport all $runDate  >> F:\\FeedManager\\Logs\\GoogleAnalytics\\DailyToplineCsvReport$(get-date -f yyyy-MM-dd).txt" + "\r\n";
                                }
                                else
                                    if (item.ToLower() == "primarydetails")
                                {
                                    text += "       & $GoogleAnalyticsAppPath DailyPrimaryCsvreport all $runDate >> F:\\FeedManager\\Logs\\GoogleAnalytics\\DailyPrimaryCsvreport$(get-date -f yyyy-MM-dd).txt" + "\r\n";
                                }
                                else
                                    if (item.ToLower() == "lcprimarydetails")
                                {
                                    text += "       & $GoogleAnalyticsAppPath LOCALCURRENCY_PRIMARYDETAILS all $runDate >> F:\\FeedManager\\Logs\\GoogleAnalytics\\LOCALCURRENCY_PRIMARYDETAILS$(get-date -f yyyy-MM-dd).txt" + "\r\n";
                                }
                                else
                                    if (item.ToLower() == "primaryextended")
                                {
                                    text += "       & $GoogleAnalyticsAppPath PRIMARYEXTENDED all $runDate >> F:\\Feedmanager\\Logs\\GoogleAnalytics\\PRIMARYEXTENDED$(get-date -f yyyy-MM-dd).txt" + "\r\n";
                                }
                                else
                                    if (item.ToLower() == "fullfunnel")
                                {
                                    text += "       & $GoogleAnalyticsAppPath FULLFUNNEL all $runDate >> F:\\Feedmanager\\Logs\\GoogleAnalytics\\FULLFUNNEL$(get-date -f yyyy-MM-dd).txt" + "\r\n";
                                }
                                else
                                    if (item.ToLower() == "funnelbyhour")
                                {
                                    text += "       & $GoogleAnalyticsAppPath FUNNELBYHOUR all $runDate >> F:\\FeedManager\\Logs\\GoogleAnalytics\\FUNNELBYHOUR$(get-date -f yyyy-MM-dd).txt" + "\r\n";
                                }
                                else
                                    if (item.ToLower() == "experiments")
                                {
                                    text += "       & $GoogleAnalyticsAppPath EXPERIMENTS all $runDate >> F:\\FeedManager\\Logs\\GoogleAnalytics\\EXPERIMENTS$(get-date -f yyyy-MM-dd).txt" + "\r\n";
                                }
                                else
                                    if (item.ToLower() == "eventerrors")
                                {
                                    text += "       & $GoogleAnalyticsAppPath EVENTERRORS all $runDate >> F:\\FeedManager\\Logs\\GoogleAnalytics\\EVENTERRORS$(get-date -f yyyy-MM-dd).txt" + "\r\n";
                                }
                                else
                                    if (item.ToLower() == "toplinebydevice")
                                {
                                    text += "       & $GoogleAnalyticsAppPath TopLineDeviceCsvReport all $runDate  >> F:\\FeedManager\\Logs\\GoogleAnalytics\\TopLineDeviceCsvReport$(get-date -f yyyy-MM-dd).txt" + "\r\n";
                                }
                                else
                                    if (item.ToLower() == "orderid_lookup")
                                {
                                    text += "       & $GoogleAnalyticsAppPath OrderID_LookUpCsvReport all $runDate >> F:\\FeedManager\\Logs\\GoogleAnalytics\\OrderID_LookUpCsvReport$(get-date -f yyyy-MM-dd).txt" + "\r\n";
                                }
                                else
                                    if (item.ToLower() == "technicaldetails")
                                {
                                    text += "       & $GoogleAnalyticsAppPath TechnicalDetailsCsvReport all $runDate >> F:\\FeedManager\\Logs\\GoogleAnalytics\\TechnicalDetailsCsvReport$(get-date -f yyyy-MM-dd).txt" + "\r\n";
                                }
                                else
                                    if (item.ToLower() == "landingpageperformance")
                                {
                                    text += "       & $GoogleAnalyticsAppPath LandingPagePerformanceCsvReport all $runDate >> F:\\FeedManager\\Logs\\GoogleAnalytics\\LANDINGPAGEPERFORMANCECsvReport$(get-date -f yyyy-MM-dd).txt" + "\r\n";
                                }
                                else
                                    if (item.ToLower() == "localcurrencytopline")
                                {
                                    text += "       & $GoogleAnalyticsAppPath LOCALCURRENCY_TOPLINE all $runDate >> F:\\FeedManager\\Logs\\GoogleAnalytics\\LOCALCURRENCY_TOPLINE$(get-date -f yyyy-MM-dd).txt" + "\r\n";
                                }
                                else
                                    if (item.ToLower() == "toplinebyminute")
                                {
                                    text += "       & $GoogleAnalyticsAppPath TopLineByMinuteCsvReport all $runDate >> F:\\FeedManager\\Logs\\GoogleAnalytics\\TopLineByMinuteCsvReport$(get-date -f yyyy-MM-dd).txt" + "\r\n";
                                }
                                else
                                    if (item.ToLower() == "localcurrencytoplinebydevice")
                                {
                                    text += "       & $GoogleAnalyticsAppPath LOCALCURRENCY_TOPLINEBYDEVICE all $runDate >> F:\\FeedManager\\Logs\\GoogleAnalytics\\LOCALCURRENCY_TOPLINEBYDEVICE$(get-date -f yyyy-MM-dd).txt" + "\r\n";
                                }
                                else
                                    if (item.ToLower() == "fullfunnel")
                                {
                                    text += "    & $GoogleAnalyticsAppPath FULLFUNNEL all $runDate >> F:\\Feedmanager\\Logs\\GoogleAnalytics\\FULLFUNNEL$(get-date -f yyyy-MM-dd).txt" + "\r\n";
                                }
                                else
                                    if (item.ToLower() == "goals")
                                {
                                    text += "    & $GoogleAnalyticsAppPath DailyGoalCsvReport all $runDate >> F:\\FeedManager\\Logs\\GoogleAnalytics\\DailyGoalCsvReport$(get-date -f yyyy-MM-dd).txt" + "\r\n";

                                }
                                else
                                    if (item.ToLower() == "productdetails")
                                {
                                    text += "       & $GoogleAnalyticsAppPath DailyProductsCsvReport all $runDate >> F:\\FeedManager\\Logs\\GoogleAnalytics\\DailyProductsCsvReport$(get-date -f yyyy-MM-dd).txt" + "\r\n";
                                }
                            }

                        }

                        text += line.ToString() + "\r\n";
                    }
                }
                // Write the PS1 file
                using (StreamWriter sw = new StreamWriter(path))
                {
                    sw.WriteLine(text);
                    //sw.WriteLine("is Extra");
                    //sw.WriteLine("Text");
                    fileName = "RunNightlyReportSashi_" + DateTime.Now.Day + ".ps1";
                    System.IO.File.WriteAllText(@"F:\\MCRM02\\PS1New\\" + fileName, text, Encoding.UTF8);

                    string strCmdText;
                    strCmdText = @"F:\MCRM02\PS1New\" + fileName;

                    ProcessStartInfo startInfo = new ProcessStartInfo();
                    // startInfo.FileName = @"powershell.exe";
                    startInfo.Arguments = strCmdText;
                    startInfo.RedirectStandardOutput = true;
                    startInfo.RedirectStandardError = true;
                    startInfo.UseShellExecute = false;
                    startInfo.CreateNoWindow = true;
                    Process process = new Process();
                    process.StartInfo.FileName = @"C:\windows\system32\windowspowershell\v1.0\powershell.exe";
                    process.StartInfo.Arguments = "\"&'" + strCmdText + "'\"";
                    process.Start();
                    process.WaitForExit();
                    message = "Files are downloaded sucessfully.";


                    //test
                }
            }
            catch (Exception ex)
            {
                message = ex.ToString();
            }

            //string strCmdText;
            //strCmdText = "C:\\Sudheer\\JdJobsDashBoard\\PS1New\\" + fileName;
            //System.Diagnostics.Process.Start("C:\\windows\\system32\\windowspowershell\\v1.0\\powershell.exe ", strCmdText);

            // System.IO.File.Delete(destFile);
            //   message = "Re-run application sucessfully.";
            return Json(message);
        }


        //Re-Run NightlyReport 
        public JsonResult RunPS1FileSite(string[] tableName, int selectedSite)
        {
            LibraryContext objContext = new LibraryContext();
            string message = string.Empty;
            string text = string.Empty;
            string sourcePath = ConfigurationManager.AppSettings["sourcePath"].ToString();
            string targetPath = ConfigurationManager.AppSettings["targetPath"].ToString();
            string fileName = string.Empty;
            string destFile = string.Empty;
            string ProfileIds = string.Empty;
            int number = 0;
            List<SiteProfile> siteProfiles = objContext.getSiteProfiles.Where(c => c.SiteId == selectedSite).ToList();
            List<SiteProfile> siteProfiles1 = siteProfiles.Where(x => int.TryParse(x.ProfileId, out number)).ToList(); // Removed alphanumarics from list
            siteProfiles1.ToList().ForEach(x => { ProfileIds += x.ProfileId + ","; });
            try
            {
                // To copy all the files in one directory to another directory.             
                if (System.IO.Directory.Exists(sourcePath))
                {
                    string[] files = System.IO.Directory.GetFiles(sourcePath);
                    // Copy the files and overwrite destination files if they already exist. 
                    foreach (string s in files)
                    {
                        // Use static Path methods to extract only the file name from the path.
                        fileName = System.IO.Path.GetFileName(s);
                        destFile = System.IO.Path.Combine(targetPath, fileName);
                        System.IO.File.Copy(s, destFile, true);
                    }
                }
                else
                {
                    message = "Source path does not exist!";
                }

                //Add checkbox checked text to PS1 file 
                string writePath = ConfigurationManager.AppSettings["WritePS1"].ToString();
                string path = Server.MapPath(writePath);
                var fileStream = new FileStream(path, FileMode.Open, FileAccess.Read);
                using (var streamReader = new StreamReader(fileStream, Encoding.UTF8))
                {
                    var lines = streamReader.ReadToEnd().Split("\r\n".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
                    foreach (var line in lines)
                    {

                        if (line.Trim().Contains("###WriteLineHere"))
                        {
                            text += "       $cmdArgList = @(" + ProfileIds.TrimEnd(',') + ")" + "\r\n";
                            text += "   foreach ($element in $cmdArgList)" + "\r\n" + "   { " + "\r\n";
                            foreach (var item in tableName)
                            {
                                if (item.ToLower() == "topline")
                                {
                                    text += "       & $GoogleAnalyticsAppPath DailyToplineCsvReport $element $runDate  >> F:\\FeedManager\\Logs\\GoogleAnalytics\\DailyToplineCsvReport$(get-date -f yyyy-MM-dd).txt" + "\r\n";
                                }
                                else
                                    if (item.ToLower() == "primarydetails")
                                {
                                    text += "       & $GoogleAnalyticsAppPath DailyPrimaryCsvreport $element $runDate >> F:\\FeedManager\\Logs\\GoogleAnalytics\\DailyPrimaryCsvreport$(get-date -f yyyy-MM-dd).txt" + "\r\n";
                                }
                                else
                                    if (item.ToLower() == "lcprimarydetails")
                                {
                                    text += "       & $GoogleAnalyticsAppPath LOCALCURRENCY_PRIMARYDETAILS $element $runDate >> F:\\FeedManager\\Logs\\GoogleAnalytics\\LOCALCURRENCY_PRIMARYDETAILS$(get-date -f yyyy-MM-dd).txt" + "\r\n";
                                }
                                else
                                    if (item.ToLower() == "primaryextended")
                                {
                                    text += "       & $GoogleAnalyticsAppPath PRIMARYEXTENDED $element $runDate >> F:\\Feedmanager\\Logs\\GoogleAnalytics\\PRIMARYEXTENDED$(get-date -f yyyy-MM-dd).txt" + "\r\n";
                                }
                                else
                                    if (item.ToLower() == "fullfunnel")
                                {
                                    text += "       & $GoogleAnalyticsAppPath FULLFUNNEL $element $runDate >> F:\\Feedmanager\\Logs\\GoogleAnalytics\\FULLFUNNEL$(get-date -f yyyy-MM-dd).txt" + "\r\n";
                                }
                                else
                                    if (item.ToLower() == "funnelbyhour")
                                {
                                    text += "       & $GoogleAnalyticsAppPath FUNNELBYHOUR $element $runDate >> F:\\FeedManager\\Logs\\GoogleAnalytics\\FUNNELBYHOUR$(get-date -f yyyy-MM-dd).txt" + "\r\n";
                                }
                                else
                                    if (item.ToLower() == "experiments")
                                {
                                    text += "       & $GoogleAnalyticsAppPath EXPERIMENTS $element $runDate >> F:\\FeedManager\\Logs\\GoogleAnalytics\\EXPERIMENTS$(get-date -f yyyy-MM-dd).txt" + "\r\n";
                                }
                                else
                                    if (item.ToLower() == "eventerrors")
                                {
                                    text += "       & $GoogleAnalyticsAppPath EVENTERRORS $element $runDate >> F:\\FeedManager\\Logs\\GoogleAnalytics\\EVENTERRORS$(get-date -f yyyy-MM-dd).txt" + "\r\n";
                                }
                                else
                                    if (item.ToLower() == "toplinebydevice")
                                {
                                    text += "       & $GoogleAnalyticsAppPath TopLineDeviceCsvReport $element $runDate  >> F:\\FeedManager\\Logs\\GoogleAnalytics\\TopLineDeviceCsvReport$(get-date -f yyyy-MM-dd).txt" + "\r\n";
                                }
                                else
                                    if (item.ToLower() == "orderid_lookup")
                                {
                                    text += "       & $GoogleAnalyticsAppPath OrderID_LookUpCsvReport $element $runDate >> F:\\FeedManager\\Logs\\GoogleAnalytics\\OrderID_LookUpCsvReport$(get-date -f yyyy-MM-dd).txt" + "\r\n";
                                }
                                else
                                    if (item.ToLower() == "technicaldetails")
                                {
                                    text += "       & $GoogleAnalyticsAppPath TechnicalDetailsCsvReport $element $runDate >> F:\\FeedManager\\Logs\\GoogleAnalytics\\TechnicalDetailsCsvReport$(get-date -f yyyy-MM-dd).txt" + "\r\n";
                                }
                                else
                                    if (item.ToLower() == "landingpageperformance")
                                {
                                    text += "       & $GoogleAnalyticsAppPath LandingPagePerformanceCsvReport $element $runDate >> F:\\FeedManager\\Logs\\GoogleAnalytics\\LANDINGPAGEPERFORMANCECsvReport$(get-date -f yyyy-MM-dd).txt" + "\r\n";
                                }
                                else
                                    if (item.ToLower() == "localcurrencytopline")
                                {
                                    text += "       & $GoogleAnalyticsAppPath LOCALCURRENCY_TOPLINE $element $runDate >> F:\\FeedManager\\Logs\\GoogleAnalytics\\LOCALCURRENCY_TOPLINE$(get-date -f yyyy-MM-dd).txt" + "\r\n";
                                }
                                else
                                    if (item.ToLower() == "toplinebyminute")
                                {
                                    text += "       & $GoogleAnalyticsAppPath TopLineByMinuteCsvReport $element $runDate >> F:\\FeedManager\\Logs\\GoogleAnalytics\\TopLineByMinuteCsvReport$(get-date -f yyyy-MM-dd).txt" + "\r\n";
                                }
                                else
                                    if (item.ToLower() == "localcurrencytoplinebydevice")
                                {
                                    text += "       & $GoogleAnalyticsAppPath LOCALCURRENCY_TOPLINEBYDEVICE $element $runDate >> F:\\FeedManager\\Logs\\GoogleAnalytics\\LOCALCURRENCY_TOPLINEBYDEVICE$(get-date -f yyyy-MM-dd).txt" + "\r\n";
                                }
                                else
                                    if (item.ToLower() == "fullfunnel")
                                {
                                    text += "    & $GoogleAnalyticsAppPath FULLFUNNEL $element $runDate >> F:\\Feedmanager\\Logs\\GoogleAnalytics\\FULLFUNNEL$(get-date -f yyyy-MM-dd).txt" + "\r\n";
                                }
                                else
                                    if (item.ToLower() == "goals")
                                {
                                    text += "    & $GoogleAnalyticsAppPath DailyGoalCsvReport $element $runDate >> F:\\FeedManager\\Logs\\GoogleAnalytics\\DailyGoalCsvReport$(get-date -f yyyy-MM-dd).txt" + "\r\n";

                                }
                                else
                                    if (item.ToLower() == "productdetails")
                                {
                                    text += "       & $GoogleAnalyticsAppPath DailyProductsCsvReport $element $runDate >> F:\\FeedManager\\Logs\\GoogleAnalytics\\DailyProductsCsvReport$(get-date -f yyyy-MM-dd).txt" + "\r\n";
                                }
                            }
                            text += "   }" + "\r\n";
                        }
                        text += line.ToString() + "\r\n";
                    }
                }
                // Write the PS1 file
                using (StreamWriter sw = new StreamWriter(path))
                {
                    sw.WriteLine(text);
                    fileName = "RunNightlyReportSashi_" + DateTime.Now.Day + ".ps1";
                    System.IO.File.WriteAllText(@"F:\\MCRM02\\PS1New\\" + fileName, text, Encoding.UTF8);
                    string strCmdText;
                    strCmdText = @"F:\MCRM02\PS1New\" + fileName;
                    ProcessStartInfo startInfo = new ProcessStartInfo();
                    startInfo.Arguments = strCmdText;
                    startInfo.RedirectStandardOutput = true;
                    startInfo.RedirectStandardError = true;
                    startInfo.UseShellExecute = false;
                    startInfo.CreateNoWindow = true;
                    Process process = new Process();
                    process.StartInfo.FileName = @"C:\windows\system32\windowspowershell\v1.0\powershell.exe";
                    process.StartInfo.Arguments = "\"&'" + strCmdText + "'\"";
                    process.Start();
                    process.WaitForExit();
                    message = "Files are downloaded sucessfully.";
                }
            }
            catch (Exception ex)
            {
                message = ex.ToString();
            }
            return Json(message);
        }
        //Dedupe NightlyReport 
        public JsonResult DedupeNightlyReport(string[] tableName)
        {
            string message = string.Empty;
            string text = string.Empty;
            string connectionString = GetConnectionString();
            string cmdText = string.Empty;

            try
            {
                //foreach (var item in tableName)
                //{
                //  string sql = GetDedupeQuery(item);
                //if (item.ToLower() == "topline")
                //{
                //    text += "       & $GoogleAnalyticsAppPath TopLineDeviceCsvReport all $runDate  >> F:\\FeedManager\\Logs\\GoogleAnalytics\\TopLineDeviceCsvReport$(get-date -f yyyy-MM-dd).txt" + "\r\n";
                //}

                //using (OracleConnection connection = new OracleConnection())
                //{
                //    connection.ConnectionString = connectionString;
                //    connection.Open();
                //    Console.WriteLine("State: {0}", connection.State);
                //    Console.WriteLine("ConnectionString: {0}",
                //                      connection.ConnectionString);

                //    OracleCommand command = connection.CreateCommand();

                //    command.CommandText = sql;

                //    OracleDataReader dr = command.ExecuteReader();

                //}

                // cmdText = GetDedupeQuery(item);
                using (OracleConnection conn = new OracleConnection(connectionString))
                using (OracleCommand cmd = new OracleCommand())
                {
                    cmd.Connection = conn;
                    cmd.CommandText = "googan.mc_Proc_GA_Dedupe";
                    cmd.CommandType = CommandType.StoredProcedure;
                    conn.Open();
                    cmd.ExecuteNonQuery();
                }
                // }
                message = "Dedupe successfully.";
            }
            catch (Exception ex)
            {
                message = ex.ToString();
            }
            return Json(message);
        }

        private string GetDedupeQuery(string table_Name)
        {
            string strQuery = string.Empty;
            if (table_Name.ToLower() == "topline")
            {
                strQuery = @"delete FROM googan.topline T
                                    where exists 
                                    (
                                    WITH
                                    PREP AS
                                    (
                                    SELECT DISTINCT
                                    SE.PROFILEID
                                    ,SE.APPLICABLEDATE
                                    ,SE.GENERATIONVERSION
                                    FROM
                                    GOOGAN.TOPLINE SE
                                    WHERE
                                    SE.APPLICABLEDATE = trunc (sysdate - 1)
                                    )
                                    ,
                                    PREP2 AS
                                    (
                                    SELECT DISTINCT
                                    PROFILEID
                                    ,APPLICABLEDATE
                                    ,GENERATIONVERSION
                                    ,ROW_NUMBER() OVER (PARTITION BY PROFILEID ORDER BY GENERATIONVERSION DESC) AS RANK
                                    FROM
                                    PREP
                                    )
                                    ,
                                    DELETIONS AS
                                    (
                                    SELECT
                                    *
                                    FROM
                                    PREP2
                                    WHERE
                                    RANK > 1
                                    )
                                    SELECT 
                                    D.PROFILEID, D.APPLICABLEDATE, D.GENERATIONVERSION 
                                    FROM DELETIONS D 
                                    WHERE T.PROFILEID = D.PROFILEID AND T.APPLICABLEDATE = D.APPLICABLEDATE AND T.GENERATIONVERSION = D.GENERATIONVERSION
                                    );
                                    ";
            }
            else if (table_Name.ToLower() == "primarydetails")
            {
                strQuery = @"delete FROM googan.primarydetails T
                                --select * FROM orderid_lookup T
                                where exists 
                                (
                                WITH
                                PREP AS
                                (
                                SELECT DISTINCT
                                SE.PROFILEID
                                ,SE.APPLICABLEDATE
                                ,SE.GENERATIONVERSION
                                FROM
                                GOOGAN.primarydetails SE
                                WHERE
                                SE.APPLICABLEDATE = trunc (sysdate - 1)
                                )
                                ,
                                PREP2 AS
                                (
                                SELECT DISTINCT
                                PROFILEID
                                ,APPLICABLEDATE
                                ,GENERATIONVERSION
                                ,ROW_NUMBER() OVER (PARTITION BY PROFILEID ORDER BY GENERATIONVERSION DESC) AS RANK
                                FROM
                                PREP
                                )
                                ,
                                DELETIONS AS
                                (
                                SELECT
                                *
                                FROM
                                PREP2
                                WHERE
                                RANK > 1
                                )
                                
                                SELECT 
                                D.PROFILEID, D.APPLICABLEDATE, D.GENERATIONVERSION 
                                FROM DELETIONS D 
                                WHERE T.PROFILEID = D.PROFILEID AND T.APPLICABLEDATE = D.APPLICABLEDATE AND T.GENERATIONVERSION = D.GENERATIONVERSION
                                );";
            }
            else if (table_Name.ToLower() == "lcprimarydetails")
            {
                strQuery = @"delete FROM googan.orderid_lookup T
                                --select * FROM orderid_lookup T
                                where exists 
                                (
                                WITH
                                PREP AS
                                (
                                SELECT DISTINCT
                                SE.PROFILEID
                                ,SE.APPLICABLEDATE
                                ,SE.GENERATIONVERSION
                                FROM
                                GOOGAN.orderid_lookup SE
                                WHERE
                                SE.APPLICABLEDATE = trunc (sysdate - 1)
                                )
                                ,
                                PREP2 AS
                                (
                                SELECT DISTINCT
                                PROFILEID
                                ,APPLICABLEDATE
                                ,GENERATIONVERSION
                                ,ROW_NUMBER() OVER (PARTITION BY PROFILEID ORDER BY GENERATIONVERSION DESC) AS RANK
                                FROM
                                PREP
                                )
                                ,
                                DELETIONS AS
                                (
                                SELECT
                                *
                                FROM
                                PREP2
                                WHERE
                                RANK > 1
                                )
                                
                                SELECT 
                                D.PROFILEID, D.APPLICABLEDATE, D.GENERATIONVERSION 
                                FROM DELETIONS D 
                                WHERE T.PROFILEID = D.PROFILEID AND T.APPLICABLEDATE = D.APPLICABLEDATE AND T.GENERATIONVERSION = D.GENERATIONVERSION)
                                
                                ;";
            }
            else if (table_Name.ToLower() == "primaryextended")
            {
                strQuery = @"";
            }
            else if (table_Name.ToLower() == "fullfunnel")
            {
                strQuery = @"";
            }
            else if (table_Name.ToLower() == "funnelbyhour")
            {
                strQuery = @"";
            }
            else if (table_Name.ToLower() == "experiments")
            {
                strQuery = @"";
            }
            else if (table_Name.ToLower() == "eventerrors")
            {
                strQuery = @"";
            }
            else if (table_Name.ToLower() == "toplinebydevice")
            {
                strQuery = @"";
            }
            else if (table_Name.ToLower() == "orderid_lookup")
            {
                strQuery = @"";
            }
            else if (table_Name.ToLower() == "technicaldetails")
            {
                strQuery = @"";
            }
            else if (table_Name.ToLower() == "landingpageperformance")
            {
                strQuery = @"";
            }
            else if (table_Name.ToLower() == "localcurrencytopline")
            {
                strQuery = @"delete   from
                                    GOOGAN.LOCALCURRENCY_topline
                                    T
                                    where exists 
                                    (
                                    WITH
                                    PREP AS
                                    (
                                    SELECT DISTINCT
                                    se.profileid
                                    ,SE.REPORTDAY
                                    ,SE.dateANDtimeGENERATED
                                    from
                                    googan.LOCALCURRENCY_topline SE
                                    WHERE
                                    se.REPORTDAY = trunc (sysdate - 1)
                                    )
                                    ,
                                    PREP2 AS
                                    (
                                    SELECT DISTINCT
                                    PROFILEID
                                    ,REPORTDAY
                                    ,dateANDtimeGENERATED
                                    ,ROW_NUMBER() OVER (PARTITION BY PROFILEID ORDER BY dateANDtimeGENERATED DESC) AS RANK
                                    FROM
                                    PREP
                                    )
                                    ,
                                    DELETIONS AS
                                    (
                                    select
                                    *
                                    FROM
                                    PREP2
                                    WHERE
                                    RANK > 1
                                    )
                                    
                                    SELECT 
                                    D.PROFILEID, D.REPORTDAY, D.dateANDtimeGENERATED 
                                    FROM DELETIONS D 
                                    WHERE T.PROFILEID = D.PROFILEID AND T.REPORTDAY = D.REPORTDAY AND T.DATEANDTIMEGENERATED = D.DATEANDTIMEGENERATED
                                    )
                                    ;";
            }
            else if (table_Name.ToLower() == "toplinebyminute")
            {
                strQuery = @"delete FROM googan.toplinebyminute T
                                --select * FROM orderid_lookup T
                                where exists 
                                (
                                WITH
                                PREP AS
                                (
                                SELECT DISTINCT
                                SE.PROFILEID
                                ,SE.APPLICABLEDATE
                                ,SE.GENERATEDVERSION
                                FROM
                                GOOGAN.toplinebyminute SE
                                WHERE
                                SE.APPLICABLEDATE = trunc (sysdate - 1)
                                )
                                ,
                                PREP2 AS
                                (
                                SELECT DISTINCT
                                PROFILEID
                                ,APPLICABLEDATE
                                ,GENERATEDVERSION
                                ,ROW_NUMBER() OVER (PARTITION BY PROFILEID ORDER BY GENERATEDVERSION DESC) AS RANK
                                FROM
                                PREP
                                )
                                ,
                                DELETIONS AS
                                (
                                SELECT
                                *
                                FROM
                                PREP2
                                WHERE
                                RANK > 1
                                )
                                
                                SELECT 
                                D.PROFILEID, D.APPLICABLEDATE, D.GENERATEDVERSION 
                                FROM DELETIONS D 
                                WHERE T.PROFILEID = D.PROFILEID AND T.APPLICABLEDATE = D.APPLICABLEDATE AND T.GENERATEDVERSION = D.GENERATEDVERSION
                                )
                                
                                ;";
            }
            return strQuery;
        }


        public ActionResult GetCountfiles()
        {

            int fileCount = Directory.GetFiles(@"C:\Sudheer\JdJobsDashBoard\PS1New").Length; // Will Retrieve count of all files in directry but not sub directries
            return Content(fileCount.ToString());
        }
    }
}




