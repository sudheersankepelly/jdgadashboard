﻿using JdJobsDashBoard.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.OracleClient;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;

namespace JdJobsDashBoard.Controllers
{
    public class MCRMCountController : Controller
    {
        // GET: MCRMCount
        public ActionResult Index()
        {
            //var items = GetMCRMData();//GetGaData();
            //if (items != null && items.Count > 0)
            //{
            //    ViewBag.GaItems = items;
            //    return View(ViewBag);
            //}
            //else
              return View();

        }
        static private string GetConnectionString()
        {
            // To avoid storing the connection string in your code, 
            // you can retrieve it from a configuration file. 
            return ConfigurationManager.AppSettings["MCRMSTGConnection"];
        }

        static private string GetQuery()
        {
            string strDate = DateTime.Now.ToString("dd-MMM-yy").ToUpper();
            string strQuery = @"SELECT 'AMPLIANCE_ASSET_DETAILS' AS TABLE_NAME, 
            count(ID) AS Total from DBO_WEBCOMMERCEINTERNAL.AMPLIANCE_ASSET_DETAILS
            where   to_char(LASTUPDATEDON) = 'DD-MON-YYYY'";

            string strQry = Regex.Replace(strQuery, "DD-MON-YYYY", strDate, RegexOptions.IgnoreCase);
            return strQry;

        }
        TaskInfo taskinf = new TaskInfo();
        //private List<TaskInfo> GetMCRMData()
        //{
        //    List<TaskInfo> LstGaReport = new List<TaskInfo>();
        //    string connectionString = GetConnectionString();
        //    string sql = GetQuery();
        //    using (OracleConnection connection = new OracleConnection())
        //    {
        //        connection.ConnectionString = connectionString;
        //        connection.Open();
        //        Console.WriteLine("State: {0}", connection.State);
        //        Console.WriteLine("ConnectionString: {0}",
        //                          connection.ConnectionString);

        //        OracleCommand command = connection.CreateCommand();

        //        command.CommandText = sql;

        //        OracleDataReader dr = command.ExecuteReader();
        //        if (dr != null && dr.HasRows)
        //        {
        //            while (dr.Read())
        //            {
        //                LstGaReport.Add(
        //                    new TaskInfo { tablename = dr["TABLE_NAME"].ToString(), total = dr["Total"].ToString() });

        //            }
        //        }
        //        connection.Close();
        //        if (LstGaReport.Count > 0)
        //        {
        //            return LstGaReport;
        //        }
        //        else
        //        {
        //            return null;
        //        }
        //    }
        //}
    }
}