﻿#* File Name: run.ps1
#*=============================================================================
#* Script Name: [Google Analytics]
#* Created:     [01/12/2014]
#* Author:      Mark James
#* Company:     JD Sports Fashion Plc
#* Email:       mark.james@jdplc.com
#*=============================================================================
#* Purpose: Run the GoogleAnalytics program
#*          
#*=============================================================================

#*=============================================================================
#* REVISION HISTORY
#*=============================================================================
#* Date:      [01/12/2014]
#* Time:      [09:41]
#* Issue:     First release
#* Solution:
#*=============================================================================

#*=============================================================================
#* SCRIPT BODY
#*=============================================================================

# Create a string variable using the local computer.
$LogFilePath=".\pslog.txt"

# Use Dot-Source to import the log functions
. F:\FeedManager\Sys\JD.FeedManager.GoogleAnalytics\logging-functions.ps1


StartLog($LogFilePath)

try 
{  	
	#Permanently remove product files > 1 year old
	
	
	# Upgraded APM 18/10/2012
	$GoogleAnalyticsAppPath = 'F:\Feedmanager\sys\JD.FeedManager.GoogleAnalytics\Instances\1.0.24 -Rerun\JD.FeedManager.GoogleAnalytics.exe'
	
	$GoogleAnalyticsSqlLoaderPath = 'F:\FeedManager\Sys\JD.FeedManager.GoogleAnalytics.SqlLoaderRunner\Instances\1.0.0.0\JD.FeedManager.GoogleAnalytics.SqlLoaderRunner.exe'
	
	$GoogleDedupePath='F:\Feedmanager\sys\JD.FeedManager.OracleGoogleAnalyticsDedupe\Instances\1.0.1\JD.FeedManager.OracleGoogleAnalyticsDedupe.exe'
		
	# Then Run Acknowledgement Job 
	 $runDate = get-date -f yyyyMMdd
	
	
	###WriteLineHere
	
	
	
	
	
	& $GoogleAnalyticsSqlLoaderPath  >> F:\FeedManager\Logs\GoogleAnalytics\SqlLoader$(get-date -f yyyy-MM-dd).txt	
	& $GoogleDedupePath   >> F:\Feedmanager\Logs\JD.FeedManager.OracleGoogleAnalyticsDedupe\GoogleDedupePath$(runDate).txt
	
	if ($LastExitCode -ne 0)  
			{throw "Error thrown by console App: $GoogleAnalyticsAppPath "}        
}  
catch 
{                
    LogErrors
}  
finally
{
    EndLog
}

#*=============================================================================
#* END OF SCRIPT: [GoogleAnalytics]
#*=============================================================================
