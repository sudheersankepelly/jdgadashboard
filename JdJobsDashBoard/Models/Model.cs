﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace JdJobsDashBoard.Models
{
    public class Model
    {

    }
    public class Sites
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string LogoUrl { get; set; }
        public string Category { get; set; }
        public string Colour { get; set; }
        public int Sequence { get; set; }
        public int ApiChannelId { get; set; }
        public int SiteGroupId { get; set; }
        public bool Enabled { get; set; }
    }
    [Table("SiteProfiles")]
    public class SiteProfile
    {
        [Key, Column(Order = 0)]
        public int SiteId { get; set; }
        [Key, Column(Order = 1)]
        public string ProfileId { get; set; }
    }
}