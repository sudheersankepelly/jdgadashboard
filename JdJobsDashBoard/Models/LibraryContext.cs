﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Web;

namespace JdJobsDashBoard.Models
{
    public class LibraryContext: DbContext
    {
        public LibraryContext()
          : base("name=GoogleAnalytics")
        {
        }
        public DbSet<Sites> getSites { get; set; }
        public DbSet<SiteProfile> getSiteProfiles { get; set; }
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
           // modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
            base.OnModelCreating(modelBuilder);
        }
    }
}