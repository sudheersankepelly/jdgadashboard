﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace JdJobsDashBoard.Models
{
    public class TaskInfo
    {
        public string folder { get; set; }
        public string name { get; set; }
        public string logFilePath { get; set; }
        public string lastRunTime { get; set; }
        public string nextRunTime { get; set; }
        public string state { get; set; }
        public string logInfo { get; set; }
        public string tablename { get; set; }
        public string schema { get; set; }
        public string total { get; set; }
    }
}