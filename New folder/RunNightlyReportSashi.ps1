﻿#* File Name: run.ps1
#*=============================================================================
#* Script Name: [Google Analytics]
#* Created:     [01/12/2014]
#* Author:      Mark James
#* Company:     JD Sports Fashion Plc
#* Email:       mark.james@jdplc.com
#*=============================================================================
#* Purpose: Run the GoogleAnalytics program
#*          
#*=============================================================================

#*=============================================================================
#* REVISION HISTORY
#*=============================================================================
#* Date:      [01/12/2014]
#* Time:      [09:41]
#* Issue:     First release
#* Solution:
#*=============================================================================

#*=============================================================================
#* SCRIPT BODY
#*=============================================================================

# Create a string variable using the local computer.
$LogFilePath=".\pslog.txt"

# Use Dot-Source to import the log functions
. F:\FeedManager\Sys\JD.FeedManager.GoogleAnalytics\logging-functions.ps1


StartLog($LogFilePath)

try 
{  	
	#Permanently remove product files > 1 year old
	
	
	# Upgraded APM 18/10/2012
	$GoogleAnalyticsAppPath = 'F:\Feedmanager\sys\JD.FeedManager.GoogleAnalytics\Instances\1.0.24 -Rerun\JD.FeedManager.GoogleAnalytics.exe'
	#$GoogleAnalyticsAppPath = 'F:\Feedmanager\sys\JD.FeedManager.GoogleAnalytics\Instances\1.0.22\JD.FeedManager.GoogleAnalytics.exe'
	#$GoogleAnalyticsAppPath = 'F:\Feedmanager\sys\JD.FeedManager.GoogleAnalytics\Instances\1.0.21\JD.FeedManager.GoogleAnalytics.exe'
	#$GoogleAnalyticsAppPath = 'F:\Feedmanager\sys\JD.FeedManager.GoogleAnalytics\Instances\1.0.20\JD.FeedManager.GoogleAnalytics.exe'
	#$GoogleAnalyticsAppPath = 'F:\FeedManager\Sys\JD.FeedManager.GoogleAnalytics\Instances\1.0.19\JD.FeedManager.GoogleAnalytics.exe'
	#$GoogleAnalyticsAppPath = 'F:\FeedManager\Sys\JD.FeedManager.GoogleAnalytics\Instances\1.0.18\JD.FeedManager.GoogleAnalytics.exe'
	#$GoogleAnalyticsAppPath = 'F:\FeedManager\Sys\JD.FeedManager.GoogleAnalytics\Instances\1.0.16\JD.FeedManager.GoogleAnalytics.exe'
	
	$GoogleAnalyticsSqlLoaderPath = 'F:\FeedManager\Sys\JD.FeedManager.GoogleAnalytics.SqlLoaderRunner\Instances\1.0.0.0\JD.FeedManager.GoogleAnalytics.SqlLoaderRunner.exe'
	
	$GoogleDedupePath='F:\Feedmanager\sys\JD.FeedManager.OracleGoogleAnalyticsDedupe\Instances\1.0.1\JD.FeedManager.OracleGoogleAnalyticsDedupe.exe'
		
	# Then Run Acknowledgement Job 
	 $runDate = get-date -f yyyyMMdd
	 #$runDate = '2018112'
	#$runDate = 'ALL'
	
	
	# & $GoogleAnalyticsAppPath LOCALCURRENCY_TOPLINE all $runDate >> F:\FeedManager\Logs\GoogleAnalytics\LOCALCURRENCY_TOPLINE$(get-date -f yyyy-MM-dd).txt
	# & $GoogleAnalyticsAppPath LOCALCURRENCY_TOPLINEBYDEVICE all $runDate >> F:\FeedManager\Logs\GoogleAnalytics\LOCALCURRENCY_TOPLINEBYDEVICE$(get-date -f yyyy-MM-dd).txt
	  #& $GoogleAnalyticsAppPath LOCALCURRENCY_PRIMARYDETAILS all $runDate >> F:\FeedManager\Logs\GoogleAnalytics\LOCALCURRENCY_PRIMARYDETAILS$(get-date -f yyyy-MM-dd).txt
	
	#& $GoogleAnalyticsAppPath DailyProductsCsvReport all $runDate >> F:\FeedManager\Logs\GoogleAnalytics\DailyProductsCsvReport$(get-date -f yyyy-MM-dd).txt
	   &$GoogleAnalyticsAppPath DailyPrimaryCsvreport all $runDate >> F:\FeedManager\Logs\GoogleAnalytics\DailyPrimaryCsvreport$(get-date -f yyyy-MM-dd).txt
	  #& $GoogleAnalyticsAppPath DailyGoalCsvReport all $runDate >> F:\FeedManager\Logs\GoogleAnalytics\DailyGoalCsvReport$(get-date -f yyyy-MM-dd).txt
	  #& $GoogleAnalyticsAppPath InternalSearchCsvReport all $runDate >> F:\FeedManager\Logs\GoogleAnalytics\InternalSearchCsvReport$(get-date -f yyyy-MM-dd).txt
	 ###& $GoogleAnalyticsAppPath MarketingConversionPaths all $runDate  >> F:\FeedManager\Logs\GoogleAnalytics\MarketingConversionPaths$(get-date -f yyyy-MM-dd).txt
	# ###& $GoogleAnalyticsAppPath MarketingInteractions all $runDate  >> F:\FeedManager\Logs\GoogleAnalytics\MarketingInteractions$(get-date -f yyyy-MM-dd).txt
	 # & $GoogleAnalyticsAppPath TopLineDeviceCsvReport all $runDate  >> F:\FeedManager\Logs\GoogleAnalytics\TopLineDeviceCsvReport$(get-date -f yyyy-MM-dd).txt
	  #& $GoogleAnalyticsAppPath DailyToplineCsvReport all $runDate  >> F:\FeedManager\Logs\GoogleAnalytics\DailyToplineCsvReport$(get-date -f yyyy-MM-dd).txt
    #& $GoogleAnalyticsAppPath OrderID_LookUpCsvReport all $runDate >> F:\FeedManager\Logs\GoogleAnalytics\OrderID_LookUpCsvReport$(get-date -f yyyy-MM-dd).txt
	  #& $GoogleAnalyticsAppPath TechnicalDetailsCsvReport all $runDate >> F:\FeedManager\Logs\GoogleAnalytics\TechnicalDetailsCsvReport$(get-date -f yyyy-MM-dd).txt
	
	  #& $GoogleAnalyticsAppPath AudienceCsvReport all $runDate >> F:\FeedManager\Logs\GoogleAnalytics\AUDIENCECsvReport$(get-date -f yyyy-MM-dd).txt
	 ###& $GoogleAnalyticsAppPath OverAllPagePerformanceCsvReport all $runDate >> F:\FeedManager\Logs\GoogleAnalytics\OVERALLPAGEPERFORMANCECsvReport$(get-date -f yyyy-MM-dd).txt
	   #& $GoogleAnalyticsAppPath LandingPagePerformanceCsvReport all $runDate >> F:\FeedManager\Logs\GoogleAnalytics\LANDINGPAGEPERFORMANCECsvReport$(get-date -f yyyy-MM-dd).txt
	
	 #& $GoogleAnalyticsAppPath TopLineByMinuteCsvReport all $runDate >> F:\FeedManager\Logs\GoogleAnalytics\TopLineByMinuteCsvReport$(get-date -f yyyy-MM-dd).txt
	
	  #& $GoogleAnalyticsAppPath GoalsByChannelCsvReport all $runDate >> F:\FeedManager\Logs\GoogleAnalytics\GoalsByChannelCsvReport$(get-date -f yyyy-MM-dd).txt
	
	 #& $GoogleAnalyticsAppPath SportsLandingPages all $runDate >> F:\FeedManager\Logs\GoogleAnalytics\SportsLandingPages$(get-date -f yyyy-MM-dd).txt
	 #& $GoogleAnalyticsSqlLoaderPath TopLineByMinute  >> F:\FeedManager\Logs\GoogleAnalytics\SqlLoaderTopLineByMinute$(get-date -f yyyy-MM-dd).txt
	  #& $GoogleAnalyticsAppPath STITCHERLANDINGPAGES all $runDate >> F:\FeedManager\Logs\GoogleAnalytics\StitcherLandingPages$(get-date -f yyyy-MM-dd).txt
	 #& $GoogleAnalyticsAppPath FUNNELMASTER all $runDate >> F:\FeedManager\Logs\GoogleAnalytics\FUNNELMASTER$(get-date -f yyyy-MM-dd).txt
	 #& $GoogleAnalyticsAppPath FULLFUNNEL all $runDate >> F:\Feedmanager\Logs\GoogleAnalytics\FULLFUNNEL$(get-date -f yyyy-MM-dd).txt
	 #& $GoogleAnalyticsAppPath EXPERIMENTS all $runDate >> F:\Feedmanager\Logs\GoogleAnalytics\EXPERIMENTS$(get-date -f yyyy-MM-dd).txt
	#& $GoogleAnalyticsAppPath PRIMARYEXTENDED all $runDate >> F:\Feedmanager\Logs\GoogleAnalytics\PRIMARYEXTENDED$(get-date -f yyyy-MM-dd).txt
	& $GoogleAnalyticsSqlLoaderPath  >> F:\FeedManager\Logs\GoogleAnalytics\SqlLoader$(get-date -f yyyy-MM-dd).txt	
	& $GoogleDedupePath   >> F:\Feedmanager\Logs\JD.FeedManager.OracleGoogleAnalyticsDedupe\GoogleDedupePath$(runDate).txt
	
	if ($LastExitCode -ne 0)  
			{throw "Error thrown by console App: $GoogleAnalyticsAppPath "}        
}  
catch 
{                
    LogErrors
}  
finally
{
    EndLog
}

#*=============================================================================
#* END OF SCRIPT: [GoogleAnalytics]
#*=============================================================================
